<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Request;
use App\Models\Book;
use App\Models\File;
use \App\Components\ImageSaver;
use Illuminate\Support\Facades\Gate;
use Session;
use Redirect;
use Validator;
use View;
use Exception;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sorted = 'asc';
        $column = 'id';
        $page = 1;
        $perPage = 15;

        if( request()->has('column') && request()->has('sorted')) {
            $column = request()->query('column');
            $sorted = request()->query('sorted');
        }

        if( request()->has('page')) {
            $page = (int) request()->query('page');
        }

        if( request()->has('perPage')) {
            $perPage = (int) request()->query('perPage');
        }

        $books = Book::orderBy($column, $sorted)->paginate($perPage)->toArray();

        return View::make('books.index')
            ->with('books', $books['data'])
            ->with('column', $column)
            ->with('sorted', $sorted)
            ->with('page', $page)
            ->with('perPage', $perPage)
            ->with('prevPage', $books['prev_page_url'])
            ->with('nextPage', $books['next_page_url'])
            ->with('firstPageUrl', $books['first_page_url'])
            ->with('lastPageUrl', $books['last_page_url'])
            ->with('lastPage', $books['last_page'])
            ->with('currentPage', $books['current_page']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check() == false) {
            return Redirect::to(url()->previous())
                ->withErrors('Only authenticated users can create books')
                ->withInput(Request::except('password'));
        }
        
        return View::make('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        if (Auth::check() == false) {
            return Redirect::to(url()->previous())
                ->withErrors('Only authenticated users can create books')
                ->withInput(Request::except('password'));
        }

        $rules = array(
            'name'           => 'required|min:1|max:255',
            'author'         => 'required|numeric',
            'pages'          => 'required|numeric',
            'images.*'       => 'required|mimes:jpeg,jpg,png',
            'descriptions.*' => 'required|min:1|max:255'
        );
        $validator = Validator::make(Request::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to(url()->previous())
                ->withErrors($validator)
                ->withInput(Request::except('password'));
        }

        $book = new Book;
        $book->name    = Request::get('name');
        $book->author  = Request::get('author');
        $book->pages   = Request::get('pages');
        $book->user_id = Auth::id();
        
        try {
            $book->save();
        } catch (Exception $e) {
            return Redirect::to(url()->previous())
            ->withErrors("Author doesn't exist..")
            ->withInput(Request::except('password'));
        }
        
        $images = Request::file('images');
        $descriptions = Request::get('descriptions');
        $filesUploaded = 0;
        $totalImages = 0;

        if($images != null) {
            $totalImages = count($images);
            $filesUploaded = ImageSaver::saveImages($images, $descriptions, $book->id);
        }
        
        Session::flash('message', 'Successfully updated book!');

        if($totalImages > 0) {
            Session::flash('message', 'Successfully updated book! '.$filesUploaded.'/'.count($images).' uploaded succesfully');
        }

        return Redirect::to('books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::findOrFail($id);
        $files = File::where('book', '=', $id)->get();

        return View::make('books.show')
            ->with('book', $book)
            ->with('files', $files);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::findOrFail($id);

        if (! Gate::allows('owns-book', $book)) {
            return Redirect::to(url()->previous())
                ->withErrors('This book does not belong to you')
                ->withInput(Request::except('password'));
        }

        $files = File::where('book', '=', $id)->get();

        return View::make('books.edit')
            ->with('book', $book)
            ->with('files', $files);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $rules = array(
            'name'           => 'required|min:1|max:255',
            'author'         => 'required|numeric',
            'pages'          => 'required|numeric',
            'images.*'       => 'required|mimes:jpeg,jpg,png',
            'descriptions.*' => 'required|min:1|max:255'
        );
        $validator = Validator::make(Request::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to(url()->previous())
                ->withErrors($validator)
                ->withInput(Request::except('password'));
        }

        $book = Book::findOrFail($id);

        if (! Gate::allows('owns-book', $book)) {
            return Redirect::to(url()->previous())
                ->withErrors('This book does not belong to you')
                ->withInput(Request::except('password'));
        }

        $book->name   = Request::get('name');
        $book->author = Request::get('author');
        $book->pages  = Request::get('pages');
        $book->save();

        $images = Request::file('images');
        $descriptions = Request::get('descriptions');
        $filesUploaded = 0;
        $totalImages = 0;
        
        if($images != null) {
            $totalImages = count($images);
            $filesUploaded = ImageSaver::saveImages($images, $descriptions, $book->id);
        }

        Session::flash('message', 'Successfully updated book!');

        if($totalImages > 0) {
            Session::flash('message', 'Successfully updated book! '.$filesUploaded.'/'.count($images).' uploaded succesfully');
        }

        return Redirect::to('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);

        if (! Gate::allows('owns-book', $book)) {
            return Redirect::to(url()->previous())
                ->withErrors('This book does not belong to you')
                ->withInput(Request::except('password'));
        }

        $book->delete();

        Session::flash('message', 'Successfully deleted the book!');
        return Redirect::to('books');
    }
}
