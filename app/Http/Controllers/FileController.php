<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;
use App\Jobs\ImageResizerJob;

class FileController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $bookId
     * @param  int  $fileId
     * @return \Illuminate\Http\Response
     */
    public function delete($bookId, $fileId)
    {
        $file = File::destroy($fileId);
        return app('App\Http\Controllers\BookController')->edit($bookId);
    }

    public static function store($image, $description, $bookId)
    {
        // Save original image
        $image->store('files', 'public');
        // Dispatch resizing of image to job
        dispatch(new ImageResizerJob($image->getRealPath(), $image->hashName()));

        $file = new File;
        $file->file_path = $image->hashName();
        $file->description = $description;
        $file->book = $bookId;
        $file->save();

        return $file;
    }
}
