<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Services\WeatherAPI;

class WeatherController extends Controller
{
    public function getWeather()
    {
        $weatherAPI = new WeatherAPI;
        $data = $weatherAPI->getWeather();
        $data = $data['daily'];
        $sorted = null;
        $column = null;

        if( request()->has('column') && request()->has('sorted')) {
            $column = request()->query('column');
            $sorted = request()->query('sorted');

            usort($data, function($first, $second)  use($sorted, $column) {
                if ($sorted == 'desc') {
                    return $first[$column] > $second[$column];
                }
                else {
                    return $first[$column] < $second[$column];
                }
            });
        }

        return view('welcome', ["data" => $data, "sorted" => $sorted, "column" => $column]);
    }
}