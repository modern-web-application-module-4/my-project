<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Author;
use Illuminate\Support\Facades\Gate;
use Session;
use Redirect;
use Validator;
use View;

class authorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sorted = 'asc';
        $column = 'id';
        $page = 1;
        $perPage = 15;

        if( request()->has('column') && request()->has('sorted')) {
            $column = request()->query('column');
            $sorted = request()->query('sorted');
        }

        if( request()->has('page')) {
            $page = (int) request()->query('page');
        }

        if( request()->has('perPage')) {
            $perPage = (int) request()->query('perPage');
        }

        $authors = Author::orderBy($column, $sorted)->paginate($perPage)->toArray();

        return View::make('authors.index')
            ->with('authors', $authors['data'])
            ->with('column', $column)
            ->with('sorted', $sorted)
            ->with('page', $page)
            ->with('perPage', $perPage)
            ->with('prevPage', $authors['prev_page_url'])
            ->with('nextPage', $authors['next_page_url'])
            ->with('firstPageUrl', $authors['first_page_url'])
            ->with('lastPageUrl', $authors['last_page_url'])
            ->with('lastPage', $authors['last_page'])
            ->with('currentPage', $authors['current_page']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (\Auth::check() == false) {
            return Redirect::to(url()->previous())
                ->withErrors('Only authenticated users can create authors')
                ->withInput(Request::except('password'));
        }

        return View::make('authors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        if (\Auth::check() == false) {
            return Redirect::to(url()->previous())
                ->withErrors('Only authenticated users can create authors')
                ->withInput(Request::except('password'));
        }

        $rules = array(
            'name'  => 'required|min:1|max:255',
            'email' => 'required|regex:/(.+)@(.+)\.(.+)/i|max:255'
        );
        $validator = Validator::make(Request::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to(url()->previous())
                ->withErrors($validator)
                ->withInput(Request::except('password'));
        } else {
            $author = new Author;
            $author->name   = Request::get('name');
            $author->email = Request::get('email');
            $author->save();

            Session::flash('message', 'Successfully created author!');
            return Redirect::to('authors');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $author = Author::findOrFail($id);

        return View::make('authors.show')
            ->with('author', $author);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $author = Author::find($id);

        if (! Gate::allows('owns-author', $author)) {
            return Redirect::to(url()->previous())
                ->withErrors('This author does not belong to you')
                ->withInput(Request::except('password'));
        }

        return View::make('authors.edit')
            ->with('author', $author);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $rules = array(
            'name'  => 'required|min:1|max:255',
            'email' => 'required|regex:/(.+)@(.+)\.(.+)/i|max:255'
        );
        $validator = Validator::make(Request::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to(url()->previous())
                ->withErrors($validator)
                ->withInput(Request::except('password'));
        }

        $author = Author::findOrFail($id);

        if (! Gate::allows('owns-author', $author)) {
            return Redirect::to(url()->previous())
                ->withErrors('This author does not belong to you')
                ->withInput(Request::except('password'));
        }

        $author->name   = Request::get('name');
        $author->author = Request::get('author');
        $author->pages  = Request::get('pages');
        $author->save();

        Session::flash('message', 'Successfully updated author!');
        return Redirect::to('authors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author = Author::findOrFail($id);

        if (! Gate::allows('owns-author', $author)) {
            return Redirect::to(url()->previous())
                ->withErrors('This author does not belong to you')
                ->withInput(Request::except('password'));
        }

        $author->delete();

        Session::flash('message', 'Successfully deleted the author!');
        return Redirect::to('authors');
    }
}
