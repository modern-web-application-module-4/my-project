<?php

namespace App\Services;

$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__ . "/../../");
$dotenv->load();

class WeatherAPI
{
    protected $url;
    protected $http;
    protected $headers;

    public function __construct()
    {   
        $client = new \GuzzleHttp\Client();
        $this->url = 'https://api.openweathermap.org/data/2.5/onecall?lat=55.676098&lon=12.568337&exclude=current,minutely,hourly,alerts&appid='.$_ENV['APP_ID'];
        $this->http = $client;
        $this->headers = [
            'cache-control' => 'no-cache',
            'content-type' => 'application/json',
        ];
    }

    public function getWeather()
    {
        # TODO Refactor should we begin to use various different URIs for the weather api.
        $full_path = $this->url;

        $request = $this->http->get($full_path, [
            'headers'         => $this->headers,
            'timeout'         => 30,
            'connect_timeout' => true,
            'http_errors'     => true,
        ]);

        $response = $request ? $request->getBody()->getContents() : null;
        $status = $request ? $request->getStatusCode() : 500;

        if ($response && $status === 200 && $response !== null) {
            return json_decode($response, true);
        }

        return null;
    }
}