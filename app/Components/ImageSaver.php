<?php

namespace App\Components;

use Request;
use App\Models\Book;
use App\Models\File;
use App\Http\Controllers\FileController;
use Session;
use Redirect;
use Validator;
use View;
use Exception;

class ImageSaver 
{
    public static function saveImages($images, $descriptions, $bookId)
    {
        $filesUploaded = 0;

        for ($i = 0; $i < count($descriptions); $i++) {
            try {
                if (array_key_exists($i, $images)) {
                    FileController::store($images[$i], $descriptions[$i], $bookId);
                    $filesUploaded++;
                }
            } catch (Exception $e) {}
        }

        return $filesUploaded;
    }
}
