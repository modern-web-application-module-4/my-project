<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration {

    /**
        * Run the migrations.
        *
        * @return void
        */
    public function up()
    {
        Schema::create('files', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('file_path');
            $table->string('description', 255);
            $table->integer('book')->unsigned();
            
            $table->timestamps();
            $table->foreign('book')->references('id')->on('books')->onDelete('cascade');
        });
    }

    /**
        * Reverse the migrations.
        *
        * @return void
        */
    public function down()
    {
        Schema::drop('files');
    }

}
 
