<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Author;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {

        $faker = \Faker\Factory::create();

        for($i = 0; $i < 100; $i++) {
            Author::create([
                'name' => $faker->userName,
                'email' => $faker->email,
                'user_id' => 1
            ]);
        }
    }
}