<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'mathias',
            'email' => 'mathias@bigler.com',
            'password' => Hash::make('123123123'),
        ]);
        event(new Registered($user));

        $user = User::create([
            'name' => 'mufasa',
            'email' => 'mufasa@bigler.com',
            'password' => Hash::make('mufasa123'),
        ]);
        event(new Registered($user));
    }
}

