<!DOCTYPE html>
<html>
<head>
    <title>book App</title>
    <link rel="stylesheet" href="{{ URL::asset('main.css') }}">
</head>
<body>

<nav class="nav">
    <ul>
        <li><a href="{{ URL::to('books') }}">View books</a></li>
        <li><a href="{{ URL::to('books/create') }}">Create book</a>
        <li><a href="{{ URL::to('authors') }}">View authors</a></li>
        <li><a href="{{ URL::to('authors/create') }}">Create author</a>
        @if (isset(Auth::user()->name))
            <li>Hello {{ Auth::user()->name }}!</li>
        @else
            <li>Hello Guest!</li>
        @endif

        @if (isset(Auth::user()->name))
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    
                    <x-dropdown-link :href="route('logout')"
                    onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-dropdown-link></li>
                </form>
            </li>
        @else
        <li>
            <form method="GET" action="{{ route('login') }}">
                @csrf
                <x-dropdown-link :href="route('login')"
                onclick="event.preventDefault();
                                    this.closest('form').submit();">
                    {{ __('Log In') }}
                </x-dropdown-link></li>
            </form>
        </li>
        @endif
    </ul>
</nav>

<h1>Showing book: {{ $book->name }}</h1>

    <div>
        <h2>{{ $book->name }}</h2>
        <p>
            <strong>Author:</strong> {{ $book->author }}<br>
            <strong>Pages:</strong> {{ $book->pages }}
        </p>
        <p>
        @foreach($files as $file)
            <a href="{{ url('storage/files/'.$file->file_path) }}">
                <img src="{{ url('storage/thumpnails/'.$file->file_path) }}"/>
            </a>
            <span>{{$file->description}}</span>
        @endforeach
        </p>
    </div>

</body>
</html>