<!DOCTYPE html>
<html>
<head>
    <title>book App</title>
    <link rel="stylesheet" href="{{ URL::asset('main.css') }}">
    <script src="{{ URL::asset('index.js')}}"></script>
</head>
<body>

<nav class="nav">
    <ul>
        <li><a href="{{ URL::to('books') }}">View books</a></li>
        <li><a href="{{ URL::to('books/create') }}">Create book</a>
        <li><a href="{{ URL::to('authors') }}">View authors</a></li>
        <li><a href="{{ URL::to('authors/create') }}">Create author</a>
        @if (isset(Auth::user()->name))
            <li>Hello {{ Auth::user()->name }}!</li>
        @else
            <li>Hello Guest!</li>
        @endif

        @if (isset(Auth::user()->name))
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    
                    <x-dropdown-link :href="route('logout')"
                    onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-dropdown-link></li>
                </form>
            </li>
        @else
        <li>
            <form method="GET" action="{{ route('login') }}">
                @csrf
                <x-dropdown-link :href="route('login')"
                onclick="event.preventDefault();
                                    this.closest('form').submit();">
                    {{ __('Log In') }}
                </x-dropdown-link></li>
            </form>
        </li>
        @endif
    </ul>
</nav>



<div class="resource-form">
    <h1>Edit book: {{ $book->name }}</h1>
    {{ Html::ul($errors->all()) }}
    {{ Form::model($book, array('id' => 'create-form', 'route' => array('books.update', $book->id), 'method' => 'PUT', 'enctype' => "multipart/form-data")) }}
    
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name') }}
    
        {{ Form::label('author', 'Author') }}
        {{ Form::text('author') }}
    
        {{ Form::label('pages', 'Pages') }}
        {{ Form::text('pages') }}

        <div id="image-block">
            <input type="file" name="images[]" multiple enctype="multipart/form-data"/>
            <input placeholder="Description of book" type="text" name="descriptions[]" multiple/>
        </div>

        <button id="add-image-block" type="button" onclick="return addImageBlock();")">Add Another Image</button>
    
        {{ Form::submit('Edit the book!') }}
    
    {{ Form::close() }}

    <h2>Existing images</h2>

    <div class="delete-images">
        @foreach($files as $file)
            <form id="delete-form" action="/files/{{$book->id}}/{{$file->id}}">
                <a href="{{ url('storage/files/'.$file->file_path) }}">
                    <img src="{{ url('storage/thumpnails/'.$file->file_path) }}"/>
                </a>
                <span>{{$file->description}}</span>
                <input type="submit" value="Delete image!">
            </form>
        @endforeach
    </div>

</div>

</body>
</html>