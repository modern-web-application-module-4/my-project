<!DOCTYPE html>
<html>
<head>
    <title>book App</title>
    <link rel="stylesheet" href="{{ URL::asset('main.css') }}">
    <script src="{{ URL::asset('index.js')}}"></script>
</head>
<body>

<nav class="nav">
    <ul>
        <li><a href="{{ URL::to('books') }}">View books</a></li>
        <li><a href="{{ URL::to('books/create') }}">Create book</a>
        <li><a href="{{ URL::to('authors') }}">View authors</a></li>
        <li><a href="{{ URL::to('authors/create') }}">Create author</a>
        @if (isset(Auth::user()->name))
            <li>Hello {{ Auth::user()->name }}!</li>
        @else
            <li>Hello Guest!</li>
        @endif

        @if (isset(Auth::user()->name))
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    
                    <x-dropdown-link :href="route('logout')"
                    onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-dropdown-link></li>
                </form>
            </li>
        @else
        <li>
            <form method="GET" action="{{ route('login') }}">
                @csrf
                <x-dropdown-link :href="route('login')"
                onclick="event.preventDefault();
                                    this.closest('form').submit();">
                    {{ __('Log In') }}
                </x-dropdown-link></li>
            </form>
        </li>
        @endif
    </ul>
</nav>



<div class="resource-form">
    
<h1>Create a book</h1>

{{ Html::ul($errors->all()) }}

{{ Form::open(array('id' => 'create-form', 'url' => 'books', 'method' => 'POST', 'enctype' => "multipart/form-data")) }}

    {{ Form::label('name', 'Name') }}
    {{ Form::text('name') }}

    {{ Form::label('author', 'Author') }}
    {{ Form::text('author') }}

    {{ Form::label('pages', 'Pages') }}
    {{ Form::text('pages') }}

    <div id="image-block">
        <input type="file" name="images[]" multiple enctype="multipart/form-data"/>
        <input placeholder="Description of book" type="text" name="descriptions[]" multiple/>
    </div>

    <button id="add-image-block" type="button" onclick="return addImageBlock();")">Add Another Image</button>
    
    {{ Form::submit('Create the book!') }}
    
    {{ Form::close() }}
    
</div>
</body>
</html>