<!DOCTYPE html>
<html>
<head>
    <title>Book App</title>
    <link rel="stylesheet" href="{{ URL::asset('main.css') }}">
</head>
<body>
<div>

<nav class="nav">
    <ul>
        <li><a href="{{ URL::to('books') }}">View books</a></li>
        <li><a href="{{ URL::to('books/create') }}">Create book</a>
        <li><a href="{{ URL::to('authors') }}">View authors</a></li>
        <li><a href="{{ URL::to('authors/create') }}">Create author</a>
        @if (isset(Auth::user()->name))
            <li>Hello {{ Auth::user()->name }}!</li>
        @else
            <li>Hello Guest!</li>
        @endif

        @if (isset(Auth::user()->name))
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    
                    <x-dropdown-link :href="route('logout')"
                    onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-dropdown-link></li>
                </form>
            </li>
        @else
        <li>
            <form method="GET" action="{{ route('login') }}">
                @csrf
                <x-dropdown-link :href="route('login')"
                onclick="event.preventDefault();
                                    this.closest('form').submit();">
                    {{ __('Log In') }}
                </x-dropdown-link></li>
            </form>
        </li>
        @endif
    </ul>
</nav>

<h1>All the books</h1>
{{ Html::ul($errors->all()) }}
<div>
    <span>Items per page: </span>
    {{ Form::open(array('url' => 'books', 'method' => "get")) }}
        <select class="per-page" name="perPage" onchange="this.form.submit()">
        
            <option value="10" {{ $perPage == 10 ? 'selected="selected"' : '' }}>10</option>    
            <option value="15" {{ $perPage == 15 ? 'selected="selected"' : '' }}>15</option>    
            <option value="20" {{ $perPage == 20 ? 'selected="selected"' : '' }}>20</option>    
        
        </select>
        <input type="hidden" name="page" value="{{$page}}">
        <input type="hidden" name="column" value="{{$column}}">
        <input type="hidden" name="sorted" value="{{$sorted}}">
    {{ Form::close() }}
</div>

@if (Session::has('message'))
    <div>{{ Session::get('message') }}</div>
@endif

<table>
    <thead>
        <tr>
            <th>ID</th>
            @if($column == 'name') 
                @if($sorted == 'asc') 
                    <th class="highlight-sorting-column">Name <a href="/books?column=name&sorted=desc">&uarr;</a></th>
                @else
                    <th class="highlight-sorting-column">Name <a href="/books?column=name&sorted=asc">&darr;</a></th>        
                @endif
            @else
                <th>Name <a href="/books?column=name&sorted=asc">&darr;</a></th>  
            @endif

            @if($column == 'author') 
                @if($sorted == 'asc') 
                    <th class="highlight-sorting-column">Author <a href="/books?column=author&sorted=desc">&uarr;</a></th>
                @else
                    <th class="highlight-sorting-column">Author <a href="/books?column=author&sorted=asc">&darr;</a></th>        
                @endif
            @else
                <th>Author <a href="/books?column=author&sorted=asc">&darr;</a></th>  
            @endif

            @if($column == 'pages') 
                @if($sorted == 'asc') 
                    <th class="highlight-sorting-column">Pages <a href="/books?column=pages&sorted=desc">&uarr;</a></th>
                @else
                    <th class="highlight-sorting-column">Pages <a href="/books?column=pages&sorted=asc">&darr;</a></th>        
                @endif
            @else
                <th>Pages <a href="/books?column=pages&sorted=asc">&darr;</a></th>  
            @endif
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($books as $book)
        <tr>
            <td>{{ $book['id'] }}</td>
            <td>{{ $book['name'] }}</td>
            <td>{{ $book['author'] }}</td>
            <td>{{ $book['pages'] }}</td>

            <td>
                <a class="a-button" href="{{ URL::to('books/' . $book['id']) }}">Show this book</a>
                <a class="a-button" href="{{ URL::to('books/' . $book['id'] . '/edit') }}">Edit this book</a>
                
                {{ Form::open(array('url' => 'books/' . $book['id'])) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    <input type="submit" value="Delete this book" onclick="return confirm('Are you sure you want to delete this book?')">
                {{ Form::close() }}

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="pagination">
    @if($prevPage == '') 
        <a class="disabled">Previous</a>
    @else
        <a href={{$prevPage}}&column={{$column}}&sorted={{$sorted}}&perPage={{$perPage}}">Previous</a>
        <a href={{$firstPageUrl}}&column={{$column}}&sorted={{$sorted}}&perPage={{$perPage}}">1</a>
    @endif
    <a class="disabled active">{{$currentPage}}</a>
    @if($nextPage == '') 
        <a class="disabled">Next</a>
    @else
        <a href="{{$lastPageUrl}}&column={{$column}}&sorted={{$sorted}}&perPage={{$perPage}}">{{$lastPage}}</a>
        <a href="{{$nextPage}}&column={{$column}}&sorted={{$sorted}}&perPage={{$perPage}}">Next</a>
    @endif
</div>

</div>
</body>
</html>