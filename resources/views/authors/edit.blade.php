<!DOCTYPE html>
<html>
<head>
    <title>author App</title>
    <link rel="stylesheet" href="{{ URL::asset('main.css') }}">
</head>
<body>

<nav class="nav">
    <ul>
        <li><a href="{{ URL::to('books') }}">View books</a></li>
        <li><a href="{{ URL::to('books/create') }}">Create book</a>
        <li><a href="{{ URL::to('authors') }}">View authors</a></li>
        <li><a href="{{ URL::to('authors/create') }}">Create author</a>
        @if (isset(Auth::user()->name))
            <li>Hello {{ Auth::user()->name }}!</li>
        @else
            <li>Hello Guest!</li>
        @endif

        @if (isset(Auth::user()->name))
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    
                    <x-dropdown-link :href="route('logout')"
                    onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-dropdown-link></li>
                </form>
            </li>
        @else
        <li>
            <form method="GET" action="{{ route('login') }}">
                @csrf
                <x-dropdown-link :href="route('login')"
                onclick="event.preventDefault();
                                    this.closest('form').submit();">
                    {{ __('Log In') }}
                </x-dropdown-link></li>
            </form>
        </li>
        @endif
    </ul>
</nav>



<div class="resource-form">
    <h1>Edit author: {{ $author->name }}</h1>
    {{ Html::ul($errors->all()) }}
    {{ Form::model($author, array('route' => array('authors.update', $author->id), 'method' => 'PUT')) }}
    
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name') }}
    
        {{ Form::label('email', 'Email') }}
        {{ Form::text('email') }}
    
        {{ Form::submit('Edit the author!') }}
    
    {{ Form::close() }}

</div>

</body>
</html>