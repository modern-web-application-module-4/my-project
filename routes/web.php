<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/', function() {
    return Redirect::to('/books');
});

# WEATHER CONTROLLER
Route::get('/weather', 'App\Http\Controllers\WeatherController@getWeather');

# BOOK CONTROLLER
Route::resource('books', 'App\Http\Controllers\BookController');

# AUTHOR CONTROLLER
Route::resource('authors', 'App\Http\Controllers\AuthorController');

# FILE CONTROLLER
Route::get('files/{bookId}/{fileId}', 'App\Http\Controllers\FileController@delete');

